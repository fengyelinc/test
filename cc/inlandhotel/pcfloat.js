﻿(function () {

    // 加载浮层js
    var version,nowTime,nowMonth,nowDay;
    nowTime = new Date();
    nowMonth = nowTime.getMonth()+1;
    nowDay = nowTime.getDate();
    if(nowMonth<10){
        nowMonth = "0"+nowMonth;
    };
    if(nowDay<10){
        nowDay = "0" + nowDay;
    };

    version = nowTime.getFullYear().toString()+nowMonth+nowDay;

    creatScript(window.location.protocol+'//webresource.c-ctrip.com/ResUnionOnline/R3/float/qCode.min.js');
    creatScript(window.location.protocol + '//webresource.c-ctrip.com/ResUnionOnline/R3/float/yUtil.min.js');
    creatScript(window.location.protocol+'//webresource.c-ctrip.com/ResUnionOnline/R3/float/floating_normal.min.js?'+version);

    function creatScript(src){
        var obj = document.createElement('script');
        obj.type = 'text/javascript';
        obj.src = src;
        document.getElementsByTagName('head')[0].appendChild(obj);
    }

    // 获取当前页面的 page_id
    var currentPageId = "";
    setTimeout(function(){
        if (document.getElementById('page_id')) {
            currentPageId = document.getElementById('page_id').value;
        }
        if (!currentPageId) {
            return;
        } else {
            showPopFloating();
        }
    },3000);
    
    
    function showPopFloating() {
        var popfloating_interval = setInterval(function () {
            try {
                if (typeof Floating === 'undefined') {
                    return;
                }
            } catch (abnormity) { }
            Floating.getPopHTML(currentPageId, getHostName(document.referrer));
            clearInterval(popfloating_interval);
        }, 100);
    }

    function getHostName(url){
        //scheme : // [username [: password] @] hostame [: port] [/ [path] [? query] [# fragment]]*/
        var regexp = new RegExp('^(?:(?:https?|ftp):)/*(?:[^@]+@)?([^:/#]+)'),
        matches = regexp.exec(url);
        return matches ? matches[1] : url;
    };
})();