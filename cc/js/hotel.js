var timer,box,control,lis,beforeIndex,index;

window.onload = function(){
	box = document.getElementById("box");	//图片框父标签
	control = document.getElementById("control");	//UL标签对象
	lis = control.children;		//li标签集合
	beforeIndex = 0;	//前置索引
	index = 0;			//当前索引
	
	//鼠标移至ul时，关闭时钟
	control.onmouseenter = function(){
		stop();
	}
	
	//鼠标离开ul时，启动时钟
	control.onmouseleave = function(){
		start();
	}
	
	//为每个li标签绑定点击事件，注意【闭包】
	for (var i = 0; i < lis.length; i++) {
		(function(i){
			lis[i].onclick = function(){
				index = i;
				change(true);
			}
		})(i);
	}
	
	//启动时钟
	start();
}

//封装切换操作
function change(move){
	if(index != beforeIndex){
		index = index>=8 ? 0 : index;
		lis[beforeIndex].className = "";	//还原前置索引li样式
		lis[index].className = "nowLi";		//修改当前索引li样式为nowLi
		beforeIndex = index;				//修改前置索引为当前索引index
		if(move){ //如果需要位置，特指：鼠标点击某li标签时
			box.style.transform = "translateX("+-(index*780)+"px)";
		}
	}
}

//启动时钟函数，每3秒执行一次
function start(){
	timer = setInterval(function(){
		box.style.transform = "translateX("+-(++index*780)+"px)";//向左位移780个像素
		//当前索引和前置索引值不同，即轮播至不同图片时
		if(index != beforeIndex){
			//如果已经轮播到最后一张图片
			if(index>=8){
				//等动画完成后操作
				setTimeout(function(){
					index = 0;	//清零偏移图片索引
					box.style.transition = "none";//关闭过渡
					box.style.transform = "translateX(0px)";//左偏移清零
				},200);
			}else{
				box.style.transition = "all 0.2s ease";//开启过渡
			}
			change(false); //切换li样式
		}
	},3000);
}

//关闭时钟函数
function stop(){
	clearInterval(timer);
}