var timer,box,control,lis,beforeIndex,index;

window.onload = function(){
	box2 = document.getElementById("box2");	//图片框父标签
	cover = document.getElementsByClassName("cover");	//UL标签对象
	lis = cover.children;		//li标签集合
	beforeIndex = 0;	//前置索引
	index = 0;			//当前索引
	
	//鼠标移至ul时，关闭时钟
	cover.onmouseenter = function(){
		stop();
	}
	
	//鼠标离开ul时，启动时钟
	cover.onmouseleave = function(){
		start();
	}
	
	//为每个li标签绑定点击事件，注意【闭包】
	for (var i = 0; i < lis.length; i++) {
		(function(i){
			lis[i].onclick = function(){
				index = i;
				change(true);
			}
		})(i);
	}
	
	//启动时钟
	start();
}

//封装切换操作
function change(move){
	if(index != beforeIndex){
		index = index>=8 ? 0 : index;
		lis[beforeIndex].className = "";	//还原前置索引li样式
		lis[index].className = "nowLi";		//修改当前索引li样式为nowLi
		beforeIndex = index;				//修改前置索引为当前索引index
		if(move){ //如果需要位置，特指：鼠标点击某li标签时
			box.style.transform = "translateX("+-(index*780)+"px)";
		}
	}
}

//延时函数3秒执行一次
function start(){
	time =setInterval(function){
		box2.style.transform = "translateX("+-(++index*780)+"px)";
		if(index!=beforeIndex){
			if(index>=5){
				setTimeout(function(){
					index = 0;	//清零偏移图片索引
					box.style.transition = "none";//关闭过渡
					box.style.transform = "translateX(0px)";//左偏移清零
				},200);
			}else{
				box.style.transition = "all 0.2s ease";//开启过渡
			}
			change(false);
		}
	},3000);
}

//关闭时钟函数
function stop(){
	clearInterval(timer);
}