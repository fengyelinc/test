package com.czhand.zsmq.api.dto;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author:WANGJING
 * @Date: 2019/3/6 14:14
 */
public class EntDTO {

	/**
	 * 企业id;关联ent_base的id
	 */
	private Long entId;

	/**
	 * 类别;类别
	 */
	private String type;

	/**
	 * 上市时间;上市时间
	 */
	private Date listDate;

	/**
	 * 股权代码;股权代码
	 */
	private String stockCode;

	/**
	 * 备注;备注
	 */
	private String remark;


	/**
	 * 企业名字
	 */
	private String entName;

	/**
	 * 企业简称
	 */
	private String entShortName;

	public Long getEntId() {
		return entId;
	}

	public void setEntId(Long entId) {
		this.entId = entId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getListDate() {
		return listDate;
	}

	public void setListDate(Date listDate) {
		this.listDate = listDate;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}

	public String getEntShortName() {
		return entShortName;
	}

	public void setEntShortName(String entShortName) {
		this.entShortName = entShortName;
	}
}
