package com.czhand.zsmq.api.controller.v1.ent;

import com.czhand.zsmq.api.dto.TalentDTO;
import com.czhand.zsmq.api.dto.ent.EntBaseDTO;
import com.czhand.zsmq.api.dto.ent.EntListDTO;
import com.czhand.zsmq.app.service.EntTalentService;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.utils.ArgsUtils;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


///**
// * @author:LVCHENBIN
// * @Date: 2019/1/22 15:18
// */
@RestController
@RequestMapping("/v1/enterprises/talent")
@Api(description = "人才企业信息维护-控制器")
public class EntTalentController {

	@Autowired
	private EntTalentService entTalentService;

//    /**
//     * 根据组织机构代码删除人才企业
//     *
//     * @param organizationalCode
//     * @return 成功返回对象
//     */
//    @ApiOperation("根据组织机构代码删除人才企业")
//    @DeleteMapping("/delete/{organizational_code}")
//    public ResponseEntity<Data<EntBaseDTO>> deleteEnterprise(
//            @PathVariable("organizational_code")
//            @ApiParam("待删除人才企业的组织机构代码") String organizationalCode) {
//        return ResponseUtils.res(new EntBaseDTO(), "33");
//    }

	/**
	 * app-人才企业
	 * @return 成功返回对象
	 */
	@ApiOperation("app-人才企业")
	@GetMapping
	public ResponseEntity<Data<List<TalentDTO>>> getAppTalent(
			@RequestParam(required = true, name = "sortParam") @ApiParam("排序参数") String sortParam,
			@RequestParam(required = true, name = "sort") @ApiParam("排序方式") String sort) {


		if (ArgsUtils.checkArgsNull(sortParam, sort)) {
			throw new CommonException("参数为空");
		}

		List<TalentDTO> talentDTOS = null;
		String message = "查询成功";
		try {
			talentDTOS =entTalentService.appTalent(sortParam,sort);
		} catch (Exception e) {
			message = "查询失败\n";
			message += e.getMessage();
		}

	return ResponseUtils.res(talentDTOS, message);

	}


}
