package com.czhand.zsmq.api.controller.v1.ent;

import com.czhand.zsmq.api.dto.ent.EntFinanceDTO;
import com.czhand.zsmq.app.service.EntFinanceService;
import com.czhand.zsmq.domain.EntFinance;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.utils.ArgsUtils;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 秦玉杰
 * @Date 2019/01/29
 * */
@RestController
@RequestMapping("/v1/enterprises/finance")
@Api(description = "财税信息维护-控制器")
public class EntFinanceController {

    @Autowired
    private EntFinanceService entFinanceService;

    /**
     * 查询所有企业财税信息
     *
     * @param
     * @return 成功返回对象
     */
    @ApiOperation("查询所有企业财税信息")
    @GetMapping("/selectAllFinance")
    public ResponseEntity<Data<List<EntFinance>>> selectAllFinance(){
        List<EntFinance> entFinanceList = new ArrayList<>();
        String message = "查询成功";
        try {
            entFinanceList = entFinanceService.selectAllFinance();
        }catch (CommonException e){
            message = "查询失败" + e.getMessage();
        }
        return ResponseUtils.res(entFinanceList,message);
    }

    /**
     * 根据企业财税信息查询
     *
     * @param entFinance
     * @return
     * */
    @ApiOperation("根据企业财税信息查询")
    @PostMapping("/selectByFinance")
    public ResponseEntity<Data<List<EntFinance>>> selectByFinance(@RequestBody @ApiParam("财税信息实体") EntFinance entFinance){
        List<EntFinance> entFinanceList = new ArrayList<>();
        String message = "查询成功";
        try {
            entFinanceList = entFinanceService.selectFinance(entFinance);
        }catch (CommonException e){
            message = "查询失败" + e.getMessage();
        }
        return ResponseUtils.res(entFinanceList,message);
    }

    /**
     * 根据年份查询企业财税信息
     *
     * @param
     * @return
     * */
    @ApiOperation("根据年份查询企业财税信息")
    @GetMapping("/selectFinanceByYear/{year}")
    public ResponseEntity<Data<List<EntFinance>>> selectFinanceByYear(@PathVariable("year") @ApiParam(value = "年份",example = "2019") String year){
        List<EntFinance> entFinanceList = new ArrayList<>();
        EntFinance entFinance = new EntFinance();
        entFinance.setYear(year);
        String message = "查询成功";
        try {
            entFinanceList = entFinanceService.selectFinanceByYear(entFinance);
        }catch (CommonException e){
            message = "查询失败" + e.getMessage();
        }
        return ResponseUtils.res(entFinanceList,message);
    }

    /**
     * 根据企业编号查询财税信息
     *
     * @param entId
     * @return 成功返回对象
     */
    @ApiOperation("根据企业编号查询当前年份财税信息")
    @GetMapping("/selectFinanceByEntId/{entId}/{year}")
    public ResponseEntity<Data<List<EntFinance>>> selectFinanceByEntId(
            @PathVariable @ApiParam(value = "企业编号",example = "14") Long entId,
            @PathVariable @ApiParam(value = "年份",example = "2019") String year){
        List<EntFinance> entFinanceList = new ArrayList<>();
        EntFinance entFinance = new EntFinance();
        entFinance.setEntId(entId);
        entFinance.setYear(year);
        String message = "查询成功";
        try {
            entFinanceList = entFinanceService.selectFinanceByEntId(entFinance);
        }catch (CommonException e){
            message = "查询失败" + e.getMessage();
        }
        return ResponseUtils.res(entFinanceList,message);
    }

    /**
     * 分页查询企业财税信息
     *
     * @param pageNo   分页查询参数pageNo
     * @param pageSize 分页查询参数pageSize
     * @return 成功返回对象
     */
    @ApiOperation("分页查询财税信息")
    @GetMapping("/queryFinancePage")
    public ResponseEntity<Data<PageInfo<EntFinance>>> queryFinancePage(
            @RequestParam(required = true, name = "pageNo") @ApiParam("当前页数") int pageNo,
            @RequestParam(required = true, name = "pageSize") @ApiParam("每页数量") int pageSize) {
        if (ArgsUtils.checkArgsNull(pageNo, pageSize)) {
            throw new CommonException("分页参数为空");
        }

        PageInfo<EntFinance> pageInfo = new PageInfo<>();
        String message = "查询成功";
        try {
            pageInfo = entFinanceService.queryFinancePage(pageNo,pageSize);
        }catch (CommonException e){
            message = "查询失败" + e.getMessage();
        }
        return ResponseUtils.res(pageInfo,message);
    }

    @ApiOperation("规模企业")
    @GetMapping("/scaleent/{year}")
    public ResponseEntity<Data<List<EntFinanceDTO>>> querryScaleEnt(
            @PathVariable @ApiParam(value = "年份", example = "2019") String year
    ) {
        List<EntFinanceDTO> entFinanceList = null;
        try {
            entFinanceList = entFinanceService.querryScaleEnt(year);
        } catch (CommonException e) {
            throw new CommonException(e.getMessage());
        }

        return ResponseUtils.res(entFinanceList);
    }

}
