package com.czhand.zsmq.api.controller.v1;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.app.service.UserService;
import com.czhand.zsmq.app.service.impl.UserServiceImpl;
import com.czhand.zsmq.domain.SysUser;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.utils.ArgsUtils;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@Api(description ="测试")
public class User2Controller {
    @Autowired
    private UserService userService;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @ApiOperation("分页查询和根据真实姓名模糊查询")
    @GetMapping("/fyByRealName")
    public PageInfo<SysUser> fyByRealName(@RequestParam(value = "realName")@ApiParam("真实姓名")String realName,
                                          @RequestParam(value = "pageNo")@ApiParam("分页查询中的参数pageNo")int pageNo,
                                          @RequestParam(value = "pageSize")@ApiParam("分页查询中的参数pageSize")int pageSize)
    {
        PageInfo<SysUser> sysUserPageInfo = null;

        try {
            sysUserPageInfo = userService.selectAllEntUser(realName, pageNo, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysUserPageInfo;
    }

    @ApiOperation("删除用户")
    @GetMapping("/deleteUser")
    public SysUserDTO deleteUser(@RequestParam(value="id") @ApiParam("根据id删除用户") Long id){
        //判断参数是否为空
        if(ArgsUtils.checkArgsNull(id)){
            throw new CommonException("参数不正确");
        }
        SysUser sysUser = userServiceImpl.deleteEntUser(id);
        if(sysUser.getIsDel()!=1){
            throw new CommonException("删除失败");
        }
        SysUserDTO sysUserDTO= ConvertHelper.convert(sysUser, SysUserDTO.class);
        return sysUserDTO;
    }

    @ApiOperation("新增用户")
    @PostMapping
    public SysUserDTO addUser(@RequestBody@RequestParam(value="sysUserDTO")@ApiParam("添加的新用户sysUserDTO") SysUserDTO sysUserDTO ,
                              @RequestParam(value="entId")@ApiParam("企业编号entId") Long entId){
        if(ArgsUtils.checkArgsNull(sysUserDTO)&&ArgsUtils.checkArgsNull(entId)){
            throw new CommonException("参数不正确");
        }
        SysUserDTO result=null;
        try {
            result = userServiceImpl.addEntUser(sysUserDTO, entId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @ApiOperation("更新用户信息")
    @GetMapping("/updateUser")
    public SysUserDTO updateUser(@RequestBody @RequestParam(value="sysUserDTO")@ApiParam("更新的用户信息sysUserDTO") SysUserDTO sysUserDTO){
        if(ArgsUtils.checkArgsNull(sysUserDTO)){
            throw new CommonException("参数不正确");
        }
        SysUserDTO result=null;
        try {
           result = userServiceImpl.updateEntUser(sysUserDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
