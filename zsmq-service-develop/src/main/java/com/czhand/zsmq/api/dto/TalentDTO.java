package com.czhand.zsmq.api.dto;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author:WANGJING
 * @Date: 2019/3/4 10:37
 */
public class TalentDTO {
	private Long id;

	/**
	 * 企业名字
	 */
	private String entName;

	/**
	 * 企业简称
	 */
	private String entShortName;
	/**
	 * 人才（团队） 人才（团队）
	 */
	private String talent;

	/**
	 * 项目 项目
	 */
	private String project;

	/**
	 * 入选时间 入选时间
	 */
	private Date selectedDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}

	public String getEntShortName() {
		return entShortName;
	}

	public void setEntShortName(String entShortName) {
		this.entShortName = entShortName;
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}
}
