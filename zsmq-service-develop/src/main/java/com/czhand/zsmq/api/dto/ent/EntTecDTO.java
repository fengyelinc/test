package com.czhand.zsmq.api.dto.ent;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@ApiModel("高新技术企业DTO")
public class EntTecDTO implements Serializable {
    /**
     * 主键id 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("主键id")
    private Long id;

    /**
     * 企业id 关联ent_base的id
     */
    @ApiModelProperty("企业id 关联ent_base的id")
    private Long entId;

    /**
     * 最后认定时间 最后认定时间
     */
    @ApiModelProperty("最后认定时间")
    private Date finalCognizanceDate;

    /**
     * 有效期 有效期
     */
    @ApiModelProperty("有效期")
    private Integer duration;

    /**
     * 有效截止日期 有效截止日期
     */
    @ApiModelProperty("有效截止日期")
    private Date expireDate;

    /**
     * 版本号 版本号（暂定为1.0）
     */
    @ApiModelProperty("版本号（暂定为1.0）")
    private Long version;

    /**
     * 创建人id 创建人id
     */
    @ApiModelProperty("创建人id")
    private Long creationBy;

    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date creationDate;

    /**
     * 更新人id 更新人id
     */
    @ApiModelProperty("更新人id")
    private Long updateBy;

    /**
     * 更新时间 更新时间
     */
    @ApiModelProperty("更新时间")
    private Date updateDate;

    /**
     * 企业名称 所有人可见
     */
    @ApiModelProperty(value = "企业名称", example = "德宜电器")
    private String entName;

    /**
     * 企业简称 所有人可见
     */
    @ApiModelProperty(value = "企业简称", example = "德电")
    private String entShortName;

    public String getEntName() {
        return entName;
    }

    public void setEntName(String entName) {
        this.entName = entName;
    }

    public String getEntShortName() {
        return entShortName;
    }

    public void setEntShortName(String entShortName) {
        this.entShortName = entShortName;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键id 主键id
     *
     * @return id - 主键id 主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键id 主键id
     *
     * @param id 主键id 主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取企业id 关联ent_base的id
     *
     * @return ent_id - 企业id 关联ent_base的id
     */
    public Long getEntId() {
        return entId;
    }

    /**
     * 设置企业id 关联ent_base的id
     *
     * @param entId 企业id 关联ent_base的id
     */
    public void setEntId(Long entId) {
        this.entId = entId;
    }

    /**
     * 获取最后认定时间 最后认定时间
     *
     * @return final_cognizance_date - 最后认定时间 最后认定时间
     */
    public Date getFinalCognizanceDate() {
        return finalCognizanceDate;
    }

    /**
     * 设置最后认定时间 最后认定时间
     *
     * @param finalCognizanceDate 最后认定时间 最后认定时间
     */
    public void setFinalCognizanceDate(Date finalCognizanceDate) {
        this.finalCognizanceDate = finalCognizanceDate;
    }

    /**
     * 获取有效期 有效期
     *
     * @return duration - 有效期 有效期
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * 设置有效期 有效期
     *
     * @param duration 有效期 有效期
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 获取有效截止日期 有效截止日期
     *
     * @return expire_date - 有效截止日期 有效截止日期
     */
    public Date getExpireDate() {
        return expireDate;
    }

    /**
     * 设置有效截止日期 有效截止日期
     *
     * @param expireDate 有效截止日期 有效截止日期
     */
    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    /**
     * 获取版本号 版本号（暂定为1.0）
     *
     * @return version - 版本号 版本号（暂定为1.0）
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 设置版本号 版本号（暂定为1.0）
     *
     * @param version 版本号 版本号（暂定为1.0）
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * 获取创建人id 创建人id
     *
     * @return creation_by - 创建人id 创建人id
     */
    public Long getCreationBy() {
        return creationBy;
    }

    /**
     * 设置创建人id 创建人id
     *
     * @param creationBy 创建人id 创建人id
     */
    public void setCreationBy(Long creationBy) {
        this.creationBy = creationBy;
    }

    /**
     * 获取创建时间 创建时间
     *
     * @return creation_date - 创建时间 创建时间
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * 设置创建时间 创建时间
     *
     * @param creationDate 创建时间 创建时间
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * 获取更新人id 更新人id
     *
     * @return update_by - 更新人id 更新人id
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人id 更新人id
     *
     * @param updateBy 更新人id 更新人id
     */
    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取更新时间 更新时间
     *
     * @return update_date - 更新时间 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间 更新时间
     *
     * @param updateDate 更新时间 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
