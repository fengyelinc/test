package com.czhand.zsmq.api.controller.v1.ent;

import com.czhand.zsmq.api.dto.ent.EntTecDTO;
import com.czhand.zsmq.app.service.EntTecService;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.utils.ArgsUtils;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author:Linjing
 * @Date: 2019/2/25
 */
@RestController
@RequestMapping("/v1/enterprises/tec")
@Api(description = "高新技术企业信息维护-控制器")
public class EntTecController {

    @Autowired
    EntTecService entTecService;

    /**
     * 查询高新技术企业
     *
     * @param sortParam
     * @param sort
     * @return
     */
    @ApiOperation("查询高新技术企业")
    @GetMapping
    public ResponseEntity<Data<List<EntTecDTO>>> selectEnterprise(
            @RequestParam(required = true, name = "sortParam") @ApiParam("排序参数") String sortParam,
            @RequestParam(required = true, name = "sort") @ApiParam("排序方式") String sort
    ) {
        if (ArgsUtils.checkArgsNull()) {
            throw new CommonException("参数不正确");
        }
        List<EntTecDTO> result = null;
        String message = "成功";
        try {
            result = entTecService.queryEntTec(sortParam, sort);
        } catch (Exception e) {
            message = e.getMessage();
        }
        return ResponseUtils.res(result, message);
    }

}
