package com.czhand.zsmq.api.controller.v1.ent;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.api.dto.ent.UserEntDTO;
import com.czhand.zsmq.app.service.EntUserService;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.utils.ArgsUtils;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author linjing
 */
@RestController
@RequestMapping("/v1/entuser")
@Api(description = "企业用户关系-控制器")
public class EntUserController {
    @Autowired
    private EntUserService entUserService;

    /**
     * 查询该企业绑定的所有用户
     *
     * @param entId 企业ID
     * @return
     */
    @ApiOperation("查询该企业绑定的所有用户")
    @GetMapping("/select/{entId}")
    public ResponseEntity<Data<PageInfo<SysUserDTO>>> selectEntUser
    (@PathVariable("entId") @ApiParam(value = "企业ID", example = "1") Long entId,
     @RequestParam(required = false, name = "realName") @ApiParam("真实姓名") String realName,
     @RequestParam(required = true, name = "pageNo") @ApiParam(value = "分页查询中的参数pageNo", example = "1") int pageNo,
     @RequestParam(required = true, name = "pageSize") @ApiParam(value = "分页查询中的参数pageSize", example = "1") int pageSize
    ) {

        PageInfo<SysUserDTO> sysUser = null;
        String message = "成功";
        try {
            sysUser = entUserService.selectEntUser(entId, pageNo, pageSize, realName);
        } catch (Exception e) {
            message = "失败";
        }
        return ResponseUtils.res(sysUser, message);
    }

    /**
     * 该用户是否已经绑定企业
     * 1.根据电话号码查询是否存在该用户，不存在则抛出异常；存在继续判断是否已经绑定企业
     * 2.1 如果已经绑定，返回该企业名称     2.2 没有绑定，返回用户信息
     *
     * @param telephone 电话号码
     * @return
     */
    @ApiOperation("查询该用户是否存在和是否已经绑定企业")
    @GetMapping("/isBand/{telephone}")
    public ResponseEntity<Data<SysUserDTO>> isBand
    (@PathVariable("telephone") @ApiParam(value = "企业ID") String telephone
    ) {
        ResponseEntity<Data<SysUserDTO>> dataResponseEntity = entUserService.isBand(telephone);
        return dataResponseEntity;
    }

    /**
     * 物理删除企业与用户的关系
     *
     * @param id 要删除的数据id
     * @return 是否删除成功，成功返回true,失败返回false
     */
    @ApiOperation("物理删除企业与用户的关系")
    @GetMapping("/delete/{id}")
    public ResponseEntity<Data<Boolean>> delete(
            @PathVariable("id") @ApiParam(value = "电话号码") Long id) {
        if (ArgsUtils.checkArgsNull(id)) {
            throw new CommonException("参数不正确");
        }
        Boolean result = false;
        String message = "成功";
        try {
            result = entUserService.delete(id);
        } catch (Exception e) {
            message = "失败";
        }
        return ResponseUtils.res(result, message);
    }

    /**
     * 插入一条企业用户关系数据
     *
     * @param userEntDTO 包括userId和entId
     * @return 插入成功的记录
     */
    @ApiOperation("插入企业与用户的关系")
    @PostMapping("/insert")
    public ResponseEntity<Data<UserEntDTO>> delete(
            @RequestBody @Valid @ApiParam("新增的企业用户关系对象") UserEntDTO userEntDTO) {
        if (ArgsUtils.checkArgsNull(userEntDTO)) {
            throw new CommonException("参数不正确");
        }
        UserEntDTO result = null;
        String message = "成功";
        try {
            result = entUserService.insert(userEntDTO);
        } catch (Exception e) {
            message = e.getMessage();
        }
        return ResponseUtils.res(result, message);
    }
}
