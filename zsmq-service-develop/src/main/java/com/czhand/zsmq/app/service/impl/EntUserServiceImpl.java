package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.api.dto.ent.UserEntDTO;
import com.czhand.zsmq.app.service.EntUserService;
import com.czhand.zsmq.domain.EntBase;
import com.czhand.zsmq.domain.SysUser;
import com.czhand.zsmq.domain.UserEnt;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.mapper.SysUserMapper;
import com.czhand.zsmq.infra.mapper.UserEntMapper;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import com.czhand.zsmq.infra.utils.convertor.ConvertPageHelper;
import com.czhand.zsmq.infra.utils.security.CurrentUserUtils;
import com.czhand.zsmq.infra.utils.web.ResponseUtils;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author linjing
 */
@Service
public class EntUserServiceImpl implements EntUserService {

    @Autowired
    private UserEntMapper userEntMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 获取 userId
     *
     * @return userId
     */
    Long getUserId() {
        return CurrentUserUtils.get().getId();
    }

    /**
     * 查询企业所有用户
     *
     * @param entId 企业ID
     * @return 该企业下的所有用户集合
     */
    @Override
    public PageInfo<SysUserDTO> selectEntUser(Long entId, int pageNo, int pageSize, String realName) {
        PageHelper.startPage(pageNo, pageSize);
        Page<SysUser> sysUserPage = (Page) userEntMapper.selectEntUser(entId, realName);
        Page<SysUserDTO> page = ConvertPageHelper.convertPage(sysUserPage, SysUserDTO.class);
        PageInfo<SysUserDTO> pageInfo = new PageInfo<>(page);
        return pageInfo;
    }

    /**
     * 物理删除企业与用户的关系
     *
     * @param id 表id
     * @return 是否删除成功
     * @throws Exception
     */
    @Override
    public Boolean delete(Long id) throws Exception {
        if (userEntMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除失败");
        }
        return true;
    }

    /**
     * 该用户是否已经绑定企业
     * 1.根据电话号码查询是否存在该用户，不存在则抛出异常；存在继续判断是否已经绑定企业
     * 2.1 如果已经绑定，返回该企业名称     2.2 没有绑定，返回用户信息
     *
     * @param telephone 用户电话
     * @return
     */
    @Override
    public ResponseEntity<Data<SysUserDTO>> isBand(String telephone) {
        String message = "成功";
        SysUser sysUser1 = sysUserMapper.isTel(telephone);
        SysUser sysUser = userEntMapper.isExit(telephone);
        try {
            if (sysUser1 == null) {
                throw new CommonException("该用户不存在");
            }
            if (sysUser.getEntName() != null && !sysUser.equals("")) {
                throw new CommonException("该用户已经绑定企业");
            }
        } catch (Exception e) {
            message = e.getMessage();
        }
        SysUserDTO sysUserDTO = null;
        if (sysUser1 != null) {
            sysUserDTO = ConvertHelper.convert(sysUser1, SysUserDTO.class);
        }
        return ResponseUtils.res(sysUserDTO, message);
    }

    /**
     * 插入数据
     * @param userEntDTO 包括userId和entId
     * @return 返回插入成功的数据
     * @throws Exception
     */
    @Override
    public UserEntDTO insert(UserEntDTO userEntDTO) throws Exception {
        UserEnt userEnt = ConvertHelper.convert(userEntDTO, UserEnt.class);
        userEnt.setVersion(1L);
        userEnt.setCreationBy(getUserId());
        userEnt.setUpdateBy(getUserId());
        userEnt.setCreationDate(new Date());
        userEnt.setUpdateDate(new Date());
        if (userEntMapper.insert(userEnt) != 1) {
            throw new CommonException("插入失败");
        }
        UserEnt userEnt1 = userEntMapper.selectByPrimaryKey(userEnt.getId());
        return ConvertHelper.convert(userEnt1, UserEntDTO.class);
    }
}
