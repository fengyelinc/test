package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.ent.EntTecDTO;
import com.czhand.zsmq.app.service.EntTecService;
import com.czhand.zsmq.domain.EntTec;
import com.czhand.zsmq.infra.mapper.EntTecMapper;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:Linjing
 * @Date: 2019/2/25
 */
@Service
public class EntTecServiceImpl implements EntTecService {

    @Autowired
    EntTecMapper entTecMapper;

    /**
     * 查询高新技术企业
     *
     * @param sortParam 排序参数
     * @param sort      排序方式
     * @return
     */
    @Override
    public List<EntTecDTO> queryEntTec(String sortParam, String sort) {
        List<EntTec> entTecList = entTecMapper.queryEntTec(sortParam, sort);
        return ConvertHelper.convertList(entTecList, EntTecDTO.class);
    }
}
