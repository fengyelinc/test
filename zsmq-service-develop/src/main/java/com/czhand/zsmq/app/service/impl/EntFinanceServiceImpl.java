package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.ent.EntFinanceDTO;
import com.czhand.zsmq.app.service.EntFinanceService;
import com.czhand.zsmq.domain.EntBase;
import com.czhand.zsmq.domain.EntFinance;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.mapper.EntBaseMapper;
import com.czhand.zsmq.infra.mapper.EntFinanceMapper;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author 秦玉杰
 * @Date 2019/01/29
 */
@Service
public class EntFinanceServiceImpl implements EntFinanceService {

    @Autowired
    private EntFinanceMapper entFinanceMapper;

    @Autowired
    private EntBaseMapper entBaseMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<EntFinance> selectAllFinance() throws CommonException {
        List<EntFinance> entFinanceList = new ArrayList<>();
        List<EntFinance> list = entFinanceMapper.selectAll();
        for (EntFinance entFinance : list) {
            EntBase entBase = entBaseMapper.selectByPrimaryKey(entFinance.getEntId());
            entFinance.setEntName(entBase.getEntName());
            entFinance.setEntShortName(entBase.getEntShortName());
            entFinanceList.add(entFinance);
        }
        return entFinanceList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<EntFinance> selectFinance(EntFinance entFinance) throws CommonException {
        List<EntFinance> entFinanceList = new ArrayList<>();
        List<EntFinance> list = entFinanceMapper.select(entFinance);
        for (EntFinance entFinance1 : list) {
            EntBase entBase = entBaseMapper.selectByPrimaryKey(entFinance1.getEntId());
            entFinance1.setEntName(entBase.getEntName());
            entFinance1.setEntShortName(entBase.getEntShortName());
            entFinanceList.add(entFinance1);
        }
        return entFinanceList;
    }

    @Override
    public List<EntFinance> selectFinanceByYear(EntFinance entFinance) throws CommonException {
        List<EntFinance> entFinanceList = new ArrayList<>();
        List<EntFinance> list = entFinanceMapper.selectFinanceByYear(entFinance);
        for (EntFinance entFinance1 : list) {
            EntBase entBase = entBaseMapper.selectByPrimaryKey(entFinance1.getEntId());
            entFinance1.setEntName(entBase.getEntName());
            entFinance1.setEntShortName(entBase.getEntShortName());
            entFinanceList.add(entFinance1);
        }
        return entFinanceList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<EntFinance> selectFinanceByEntId(EntFinance entFinance) throws CommonException {
        EntBase entBase = new EntBase();
        entBase.setId(entFinance.getEntId());
        EntBase entBase1 = entBaseMapper.selectByPrimaryKey(entBase);
        if (entBase1 == null) {
            throw new CommonException("企业不存在");
        }
        List<EntFinance> entFinanceList = new ArrayList<>();
        List<EntFinance> entFinanceList1 = entFinanceMapper.selectFinanceByEntId(entFinance);
        for (EntFinance entFinance1 : entFinanceList1) {
            entFinance1.setEntName(entBase1.getEntName());
            entFinance1.setEntShortName(entBase1.getEntShortName());
            entFinanceList.add(entFinance1);
        }
        return entFinanceList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public PageInfo<EntFinance> queryFinancePage(int pageNo, int pageSize) throws CommonException {
        PageHelper.startPage(pageNo, pageSize);
        Page<EntFinance> entFinancePage1 = (Page) entFinanceMapper.selectAll();
        Page<EntFinance> entFinancePage = new Page<>();
        for (EntFinance entFinance : entFinancePage1) {
            EntBase entBase = entBaseMapper.selectByPrimaryKey(entFinance.getEntId());
            entFinance.setEntName(entBase.getEntName());
            entFinance.setEntShortName(entBase.getEntShortName());
            entFinancePage.add(entFinance);
        }
        PageInfo<EntFinance> pageInfo = new PageInfo<>(entFinancePage);
        return pageInfo;
    }

    @Override
    public List<EntFinanceDTO> querryScaleEnt(String year) throws CommonException {
        //取得纳税排序
        List<Long> eIdList = entFinanceMapper.querryTaxesRow(year);
        HashMap<Long, Integer> taxesRow = new HashMap<>();
        int trow = 1;
        for (Long entId : eIdList) {
            taxesRow.put(entId, trow);
            trow++;
        }

        //取得销售排序
        List<Long> entIdList = entFinanceMapper.querrySaleRow(year);
        //组合数据
        List<EntFinance> entFinanceList = new ArrayList<>();

        int srow = 1;
        for (Long entId : entIdList) {
            EntFinance entFinance = new EntFinance();
            entFinance.setSaleRow((srow));
            entFinance.setEntId(entId);
            EntFinance ef = entFinanceMapper.querryScaleEnt(year, entId);
            entFinance.setEntName(ef.getEntName());
            entFinance.setEntShortName(ef.getEntShortName());
            entFinance.setSale(ef.getSale());
            entFinance.setTaxes(ef.getTaxes());
            entFinance.setTaxesRow(taxesRow.get(entId));
            entFinanceList.add(entFinance);
            srow++;
        }
        return ConvertHelper.convertList(entFinanceList, EntFinanceDTO.class);
    }

}
