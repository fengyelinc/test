package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.TalentDTO;
import com.czhand.zsmq.app.service.EntTalentService;
import com.czhand.zsmq.infra.mapper.EntTalentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:WANGJING
 * @Date: 2019/3/4 10:57
 */

@Service
public class EntTalentServiceImpl implements EntTalentService {

	@Autowired
	private EntTalentMapper entTalentMapper;

	@Override
	public List<TalentDTO> appTalent(String sortParam, String sort) {
		return entTalentMapper.appTalent(sortParam,sort);
	}
}

