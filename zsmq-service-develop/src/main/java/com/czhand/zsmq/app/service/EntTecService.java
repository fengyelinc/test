package com.czhand.zsmq.app.service;


import com.czhand.zsmq.api.dto.ent.EntTecDTO;

import java.util.List;

/**
 * @author:Linjing
 * @Date: 2019/2/25
 */
public interface EntTecService {
    /**
     * 查询高新技术企业
     *
     * @param sortParam 排序参数
     * @param sort      排序方式
     * @return
     */
    List<EntTecDTO> queryEntTec(String sortParam, String sort);
}
