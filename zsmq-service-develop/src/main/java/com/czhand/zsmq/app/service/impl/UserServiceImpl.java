package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.app.service.UserService;
import com.czhand.zsmq.domain.SysUser;
import com.czhand.zsmq.domain.UserEnt;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.mapper.SysUserMapper;
import com.czhand.zsmq.infra.mapper.UserEntMapper;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserEntMapper userEntMapper;



    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysUserDTO addEntUser(SysUserDTO sysUserDTO, Long entId) {
        SysUser sysUser = ConvertHelper.convert(sysUserDTO, SysUser.class);
        //判断登陆名是否唯一
        if(sysUserMapper.isUserName(sysUser.getUserName())!=null){
            throw new CommonException("用户已存在");
        }
        //判断电话号码是否唯一
        if(sysUserMapper.isTel(sysUser.getTelephone())!=null){
            throw new CommonException("号码已被注册");
        }
        //加密密码
        sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
        sysUser.setVersion(1L);
        sysUser.setIsDel(0);
        sysUser.setCreationDate(new Date());
        sysUser.setUpdateDate(new Date());
        int result = sysUserMapper.insertSelective(sysUser);
        if (result != 1 && sysUser.getId() == null) {
            throw new CommonException("创建用户失败");
        }

        UserEnt userEnt = new UserEnt();
        userEnt.setEntId(entId);
        userEnt.setUserId(sysUser.getId());
        userEnt.setVersion(1L);
        userEnt.setCreationDate(new Date());
        userEnt.setUpdateDate(new Date());

        if(userEntMapper.insertSelective(userEnt)!=1){
            throw new CommonException("创建用户失败");
        }

        SysUserDTO sysUserDTOResult = ConvertHelper.convert(sysUserMapper.selectByPrimaryKey(sysUser.getId()), SysUserDTO.class);


        return sysUserDTOResult;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysUser deleteEntUser(Long id) {
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        SysUser sysUser1 = sysUserMapper.selectByPrimaryKey(sysUser);
        int result=0;
        if(sysUser1!=null){
            sysUser.setIsDel(1);
            result = sysUserMapper.updateByPrimaryKey(sysUser);
            if(result!=1){
                throw new CommonException("删除失败");
            }
        }else{
            throw new CommonException("用户不存在");
        }
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysUserDTO updateEntUser(SysUserDTO sysUserDTO) {
        SysUser sysUser = ConvertHelper.convert(sysUserDTO,SysUser.class);
        int result=0;
        if(sysUser!=null){
            result = sysUserMapper.updateByPrimaryKeySelective(sysUser);
            if(result!=1){
                throw new CommonException("更新失败");
            }
        }else{
            throw new CommonException("数据不存在");
        }
        SysUserDTO sysUserDTO1 = ConvertHelper.convert(sysUser, SysUserDTO.class);
        return sysUserDTO1;
    }

    @Override
    public PageInfo<SysUser> selectAllEntUser(String realName,int pageNo, int pageSize) {
        PageHelper.startPage(pageNo,pageSize);
        Page<SysUser> sysUsersList=null;
         if(realName==null||realName.equals("")){
             sysUsersList= (Page<SysUser>) sysUserMapper.selectAll();
         }else{
             sysUsersList=(Page<SysUser>) sysUserMapper.selectByRealName(realName);
         }
        PageInfo<SysUser> pageInfo = new PageInfo<>(sysUsersList);

        return pageInfo;
    }


}
