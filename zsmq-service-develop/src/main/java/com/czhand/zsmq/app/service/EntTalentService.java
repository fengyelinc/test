package com.czhand.zsmq.app.service;

import com.czhand.zsmq.api.dto.TalentDTO;

import java.util.List;

/**
 * @author WANGJING
 */
public interface EntTalentService {
	/**
	 * 查询人才企业信息
	 * App端
	 */
	List<TalentDTO> appTalent(String sortParam, String sort);
}
