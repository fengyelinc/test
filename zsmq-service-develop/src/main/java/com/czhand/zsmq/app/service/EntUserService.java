package com.czhand.zsmq.app.service;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.api.dto.ent.UserEntDTO;
import com.czhand.zsmq.infra.utils.web.dto.Data;
import com.github.pagehelper.PageInfo;
import org.springframework.http.ResponseEntity;


/**
 * @author linjing
 */
public interface EntUserService {
    /**
     * 查询企业所有用户
     *
     * @param entId    企业Id
     * @param pageNo   分页查询当前页
     * @param pageSize 分页查询一页显示的数量
     * @param realName 用户真实姓名
     * @return 绑定该企业的所有用户
     */
    PageInfo<SysUserDTO> selectEntUser(Long entId, int pageNo, int pageSize, String realName);

    /**
     * 删除该用户与企业的绑定关系
     *
     * @param id 表id
     * @return 是否删除成功
     */
    Boolean delete(Long id) throws Exception;

    /**
     * 该用户是否已经绑定企业
     * 1.根据电话号码查询是否存在该用户，不存在则抛出异常；存在继续判断是否已经绑定企业
     * 2.1 如果已经绑定，返回该企业名称     2.2 没有绑定，返回用户信息
     *
     * @param telephone  用户电话
     * @return
     */
    ResponseEntity<Data<SysUserDTO>> isBand(String telephone);

    /**
     * 插入数据
     *
     * @param userEntDTO 包括userId和entId
     * @return 返回插入成功的数据
     */
    UserEntDTO insert(UserEntDTO userEntDTO) throws Exception;

}
