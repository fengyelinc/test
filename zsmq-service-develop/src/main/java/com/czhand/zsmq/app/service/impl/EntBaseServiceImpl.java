package com.czhand.zsmq.app.service.impl;

import com.czhand.zsmq.api.dto.ent.EntBaseDTO;
import com.czhand.zsmq.app.service.EntBaseService;
import com.czhand.zsmq.domain.EntBase;
import com.czhand.zsmq.domain.core.CurrentUser;
import com.czhand.zsmq.infra.exception.CommonException;
import com.czhand.zsmq.infra.mapper.EntBaseMapper;
import com.czhand.zsmq.infra.utils.ExcelUtil;
import com.czhand.zsmq.infra.utils.convertor.ConvertHelper;
import com.czhand.zsmq.infra.utils.convertor.ConvertPageHelper;
import com.czhand.zsmq.infra.utils.security.CurrentUserUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author:LVCHENBIN
 * @Date: 2019/1/22 16:38
 */
@Service
public class EntBaseServiceImpl implements EntBaseService {

    @Autowired
    private EntBaseMapper entBaseMapper;

    public Long getUserId() {
        CurrentUser currentUser = CurrentUserUtils.get();
        Long userId = currentUser.getId();
        return userId;
    }

    /**
     * 创建企业
     *
     * @param entBase
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public EntBaseDTO createEnterprise(EntBaseDTO entBase) throws CommonException {

        entBase.setIsDel(0);
        entBase.setVersion(1L);
        entBase.setCreationBy(getUserId());
        entBase.setCreationDate(new Date());
        entBase.setUpdateBy(getUserId());
        entBase.setUpdateDate(new Date());

        //当插入企业已存在时（组织机构代码）
        int uniqueResult = entBaseMapper.queryExist(ConvertHelper.convert(entBase, EntBase.class));
        if (uniqueResult != 0) {
            //查询出该企业
            EntBase entBase2 = entBaseMapper.queryOne(entBase.getOrganizationalCode());
            EntBaseDTO entBaseOld = ConvertHelper.convert(entBase2, EntBaseDTO.class);
            //若该企业已删除，用新信息更新
            int isDel = entBaseOld.getIsDel();
            if (isDel == 1) {
                return updateEnterprise(entBase);
            }
            throw new CommonException("当前插入企业已存在！");
        }

        int result = entBaseMapper.insert(ConvertHelper.convert(entBase, EntBase.class));
        String organizationalCode = entBase.getOrganizationalCode();
        if (result != 1) {
            throw new CommonException("插入数据失败！");
        }
        return ConvertHelper.convert(entBaseMapper.queryOne(organizationalCode), EntBaseDTO.class);
    }

    /**
     * 更新企业
     *
     * @param entBase
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public EntBaseDTO updateEnterprise(EntBaseDTO entBase) throws CommonException {

        //根据组织机构代码，获取旧数据的id+版本
        EntBaseDTO entBase2 = ConvertHelper.convert(
                entBaseMapper.queryOne(entBase.getOrganizationalCode()), EntBaseDTO.class);
        long versionOld = entBase2.getVersion();
        long idOld = entBase2.getId();

        entBase.setId(idOld);
        entBase.setVersion(versionOld + 1);
        entBase.setCreationBy(entBase2.getCreationBy());
        entBase.setCreationDate(entBase2.getCreationDate());
        entBase.setUpdateBy(getUserId());
        entBase.setUpdateDate(new Date());

        int result = entBaseMapper.updateByPrimaryKeySelective(ConvertHelper.convert(entBase, EntBase.class));
        Long Id = entBase.getId();
        if (result != 1) {
            throw new CommonException("更新数据失败！");
        }
        return ConvertHelper.convert(entBaseMapper.selectByPrimaryKey(Id), EntBaseDTO.class);
    }

    /**
     * 删除企业
     *
     * @param organizationalCode
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public EntBaseDTO deleteEnterprise(String organizationalCode) throws CommonException {

        //根据组织机构代码，获取旧数据的id+版本
        EntBaseDTO entBase2 = ConvertHelper.convert(
                entBaseMapper.queryOne(organizationalCode), EntBaseDTO.class);
        long versionOld = entBase2.getVersion();

        //设置需删除(更新)的DTO
        entBase2.setIsDel(1);
        entBase2.setVersion(versionOld + 1);
        entBase2.setUpdateBy(getUserId());
        entBase2.setUpdateDate(new Date());

        int result = entBaseMapper.updateByPrimaryKeySelective(ConvertHelper.convert(entBase2, EntBase.class));
        Long Id = entBase2.getId();

        if (result != 1) {
            throw new CommonException("删除数据失败！");
        }

        return ConvertHelper.convert(entBaseMapper.selectByPrimaryKey(Id), EntBaseDTO.class);
    }


    /**
     * 查询单个企业
     *
     * @param organizationalCode
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public EntBaseDTO queryOneEnterprise(String organizationalCode) throws CommonException {

        return ConvertHelper.convert(entBaseMapper.queryOne(organizationalCode), EntBaseDTO.class);
    }

    /**
     * 查询所有企业
     */
    @Override
    public PageInfo<EntBaseDTO> queryAllEnterprise(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        Page<EntBase> entBaseList = (Page) entBaseMapper.queryAll();
        Page<EntBaseDTO> page = ConvertPageHelper.convertPage(entBaseList, EntBaseDTO.class);
        PageInfo<EntBaseDTO> pageInfo = new PageInfo<>(page);
        return pageInfo;
    }

    /**
     * 查询企业，by模糊匹配企业名称
     */
    @Override
    public PageInfo<EntBaseDTO> queryAllByEntName(String entName, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        EntBase entBase = new EntBase();
        entBase.setEntName(entName);
        Page<List<EntBase>> entBaseList = (Page) entBaseMapper.queryAllByEntName(entBase);
        Page<EntBaseDTO> page = ConvertPageHelper.convertPage(entBaseList, EntBaseDTO.class);
        PageInfo<EntBaseDTO> pageInfo = new PageInfo<>(page);
        return pageInfo;
    }

    /**
     * 查询企业是否重复
     *
     * @param entBase
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean queryExistEnterprise(EntBaseDTO entBase) throws CommonException {

        int uniqueResult = entBaseMapper.queryExist(ConvertHelper.convert(entBase, EntBase.class));
        boolean result = true;
        if (uniqueResult == 0) {
            result = false;
        }
        return result;
    }

    /**
     * 查询企业ID
     *
     * @param organizationalCode
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public long queryEntId(String organizationalCode) throws CommonException {

        long entId = entBaseMapper.queryEntId(organizationalCode);

        return entId;
    }

    @Override
    public EntBase enableEntBase(String organizationalCode, int isdel) throws CommonException {
        EntBase entBase = new EntBase();
        entBase.setIsDel(isdel);
        entBase.setOrganizationalCode(organizationalCode);
        entBase.setUpdateBy(getUserId());
        int result = entBaseMapper.enableEntBase(entBase);
        if (result != 1 && result != 0) {
            throw new CommonException("操作失败");
        }
        return entBaseMapper.selectOne(entBase);
    }

    @Override
    public Boolean excelImport(MultipartFile file) {
        boolean flag = false;
        List<EntBaseDTO> entBaseDTOList = new ArrayList<>();
        try {
            String fileName = file.getOriginalFilename();
            InputStream is = file.getInputStream();//转化为流的形式
            List<Row> list = ExcelUtil.getExcelRead(fileName, is, true);
            //首先是读取行 也就是一行一行读，然后在取到列，遍历行里面的行，根据行得到列的值
            for (Row row : list) {
                Cell cell_0 = row.getCell(0);
                Cell cell_1 = row.getCell(1);
                Cell cell_2 = row.getCell(2);
                Cell cell_3 = row.getCell(3);
                Cell cell_4 = row.getCell(4);
                Cell cell_5 = row.getCell(5);
                Cell cell_6 = row.getCell(6);
                Cell cell_7 = row.getCell(7);
                Cell cell_8 = row.getCell(8);
                Cell cell_9 = row.getCell(9);
                Cell cell_10 = row.getCell(10);
                Cell cell_11 = row.getCell(11);
                Cell cell_12 = row.getCell(12);
                Cell cell_13 = row.getCell(13);
                Cell cell_14 = row.getCell(14);
                Cell cell_15 = row.getCell(15);

                //得到列的值，也就是你需要解析的字段的值
                String entName = ExcelUtil.getValue(cell_0);
                String entShortName = ExcelUtil.getValue(cell_1);
                String entAbstract = ExcelUtil.getValue(cell_2);
                String address = ExcelUtil.getValue(cell_3);
                String corporation = ExcelUtil.getValue(cell_4);
                String corporationTel = ExcelUtil.getValue(cell_5);
                String registeredCapital = ExcelUtil.getValue(cell_6);
                //成立日期，转Date类型
                String establishDateStr = ExcelUtil.getValue(cell_7);
                Date establishDate = cell_7.getDateCellValue();
                String registrationNum = ExcelUtil.getValue(cell_8);
                String organizationalCode = ExcelUtil.getValue(cell_9);
                String shareholder = ExcelUtil.getValue(cell_10);
                String contacts = ExcelUtil.getValue(cell_11);
                String contactsTel = ExcelUtil.getValue(cell_12);
                String siteArea = ExcelUtil.getValue(cell_13);
                String landArea = ExcelUtil.getValue(cell_14);
                String propertyArea = ExcelUtil.getValue(cell_15);

                //将读取出来的数值进行包装
                EntBaseDTO entBase = new EntBaseDTO();
                entBase.setEntName(entName);
                entBase.setEntShortName(entShortName);
                entBase.setEntAbstract(entAbstract);
                entBase.setAddress(address);
                entBase.setCorporation(corporation);
                entBase.setCorporationTel(corporationTel);
                entBase.setRegisteredCapital(registeredCapital);
                entBase.setEstablishDate(establishDate);
                entBase.setRegistrationNum(registrationNum);
                entBase.setOrganizationalCode(organizationalCode);
                entBase.setShareholder(shareholder);
                entBase.setContacts(contacts);
                entBase.setContactsTel(contactsTel);
                entBase.setSiteArea(siteArea);
                entBase.setLandArea(landArea);
                entBase.setPropertyArea(propertyArea);
                entBase.setIsDel(0);
                entBase.setVersion(1L);
                entBase.setCreationBy(getUserId());
                entBase.setCreationDate(new Date());
                entBase.setUpdateBy(getUserId());
                entBase.setUpdateDate(new Date());
                entBaseDTOList.add(entBase);
            }
            if (entBaseDTOList.size() > 0) {
                int result = entBaseMapper.importInsertEntBase(entBaseDTOList);
                if(result < 0){
                    throw new CommonException("数据导入失败");
                }else {
                    flag = true;
                }
            }
        } catch (Exception e) {
            throw new CommonException(e.getMessage());
        }
        return flag;
    }
}
