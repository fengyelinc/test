package com.czhand.zsmq.app.service;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.domain.SysUser;
import com.czhand.zsmq.domain.UserEnt;

import java.util.List;

public interface UserEntService {

    SysUserDTO addEntUser(SysUserDTO sysUserDTO,Long entId);


}
