package com.czhand.zsmq.app.service;

import com.czhand.zsmq.api.dto.SysUserDTO;
import com.czhand.zsmq.domain.SysUser;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    //添加用户
    SysUserDTO addEntUser(SysUserDTO sysUserDTO,Long entId);
    //删除用户
    SysUser deleteEntUser(Long id);
    //修改用户
    SysUserDTO updateEntUser(SysUserDTO sysUserDTO);
    //分页查询用户信息
    PageInfo<SysUser> selectAllEntUser(String realName,int pageNo, int pageSize);
}
