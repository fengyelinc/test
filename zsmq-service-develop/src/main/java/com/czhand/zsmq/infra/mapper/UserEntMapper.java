package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.SysUser;
import com.czhand.zsmq.domain.UserEnt;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserEntMapper extends BaseMapper<UserEnt> {
    /**
     * 查询该企业绑定的所有用户
     * @param entId 企业id
     * @param realName  真实姓名
     * @return
     */
    List<SysUser> selectEntUser(@Param("entId") Long entId, @Param("realName") String realName);

    /**
     * 根据该用户电话查询是否已经绑定企业
     * @param telephone 电话号码
     * @return
     */
    SysUser isExit(String telephone);
}