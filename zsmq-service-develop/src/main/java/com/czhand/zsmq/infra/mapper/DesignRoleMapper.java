package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.DesignRole;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;

public interface DesignRoleMapper extends BaseMapper<DesignRole> {
}