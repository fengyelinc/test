package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.api.dto.TalentDTO;
import com.czhand.zsmq.domain.EntTalent;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface EntTalentMapper extends BaseMapper<EntTalent> {
	List<TalentDTO> appTalent(@Param("sortParam") String sortParam, @Param("sort") String sort);
}