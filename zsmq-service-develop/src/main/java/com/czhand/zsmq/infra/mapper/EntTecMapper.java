package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.EntTec;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author:Linjing
 * @Date: 2019/2/25
 */
public interface EntTecMapper extends BaseMapper<EntTec> {
    /**
     * 查询高新技术企业
     *
     * @param sortParam 排序参数
     * @param sort      排序方式
     * @return
     */
    List<EntTec> queryEntTec(@Param("sortParam") String sortParam, @Param("sort") String sort);
}