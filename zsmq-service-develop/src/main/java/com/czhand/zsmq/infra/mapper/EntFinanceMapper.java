package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.EntFinance;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;


@Component
public interface EntFinanceMapper extends BaseMapper<EntFinance> {

    /**
     * 查询规模企业
     * @param year
     * @param entId
     * @return
     */
    EntFinance querryScaleEnt(@Param("year") String year,@Param("entId")Long entId);

    /**
     * 销售额排序
     * @param year
     * @return
     */
    List<Long> querrySaleRow(String year);

    /**
     * 纳税额排序
     * @param year
     * @return
     */
    List<Long> querryTaxesRow(@Param("year") String year);


    List<EntFinance> selectFinanceByYear(EntFinance entFinance);

    List<EntFinance> selectFinanceByEntId(EntFinance entFinance);

}
