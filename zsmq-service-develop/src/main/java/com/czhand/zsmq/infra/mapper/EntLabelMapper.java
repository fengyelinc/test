package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.EntLabel;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;

public interface EntLabelMapper extends BaseMapper<EntLabel> {
}