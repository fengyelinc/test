package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.DesignTenant;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;

public interface DesignTenantMapper extends BaseMapper<DesignTenant> {
}