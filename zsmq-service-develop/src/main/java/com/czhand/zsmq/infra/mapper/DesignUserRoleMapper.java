package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.DesignUserRole;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;

public interface DesignUserRoleMapper extends BaseMapper<DesignUserRole> {
}