package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.api.dto.EntDTO;
import com.czhand.zsmq.domain.EntList;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface EntListMapper extends BaseMapper<EntList> {

    EntList selectByEntId(long entId);


	List<EntDTO> queryEntAll(@Param("sortParam") String sortParam, @Param("sort") String sort);


}
