package com.czhand.zsmq.infra.mapper;

import com.czhand.zsmq.domain.DesignUser;
import com.czhand.zsmq.infra.utils.mapper.BaseMapper;

public interface DesignUserMapper extends BaseMapper<DesignUser> {
}