package com.czhand.zsmq.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "ent_talent")
public class EntTalent implements Serializable {
    /**
     * 主键id 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 企业id 关联ent_base的id
     */
    @Column(name = "ent_id")
    private Long entId;

    /**
     * 人才（团队） 人才（团队）
     */
    private String talent;

    /**
     * 项目 项目
     */
    private String project;

    /**
     * 入选时间 入选时间
     */
    @Column(name = "selected_date")
    private Date selectedDate;

    /**
     * 版本号 版本号（暂定为1.0）
     */
    private Long version;

    /**
     * 创建人id 创建人id
     */
    @Column(name = "creation_by")
    private Long creationBy;

    /**
     * 创建时间 创建时间
     */
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * 更新人id 更新人id
     */
    @Column(name = "update_by")
    private Long updateBy;

    /**
     * 更新时间 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键id 主键id
     *
     * @return id - 主键id 主键id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键id 主键id
     *
     * @param id 主键id 主键id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取企业id 关联ent_base的id
     *
     * @return ent_id - 企业id 关联ent_base的id
     */
    public Long getEntId() {
        return entId;
    }

    /**
     * 设置企业id 关联ent_base的id
     *
     * @param entId 企业id 关联ent_base的id
     */
    public void setEntId(Long entId) {
        this.entId = entId;
    }

    /**
     * 获取人才（团队） 人才（团队）
     *
     * @return talent - 人才（团队） 人才（团队）
     */
    public String getTalent() {
        return talent;
    }

    /**
     * 设置人才（团队） 人才（团队）
     *
     * @param talent 人才（团队） 人才（团队）
     */
    public void setTalent(String talent) {
        this.talent = talent;
    }

    /**
     * 获取项目 项目
     *
     * @return project - 项目 项目
     */
    public String getProject() {
        return project;
    }

    /**
     * 设置项目 项目
     *
     * @param project 项目 项目
     */
    public void setProject(String project) {
        this.project = project;
    }

    /**
     * 获取入选时间 入选时间
     *
     * @return selected_date - 入选时间 入选时间
     */
    public Date getSelectedDate() {
        return selectedDate;
    }

    /**
     * 设置入选时间 入选时间
     *
     * @param selectedDate 入选时间 入选时间
     */
    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    /**
     * 获取版本号 版本号（暂定为1.0）
     *
     * @return version - 版本号 版本号（暂定为1.0）
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 设置版本号 版本号（暂定为1.0）
     *
     * @param version 版本号 版本号（暂定为1.0）
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * 获取创建人id 创建人id
     *
     * @return creation_by - 创建人id 创建人id
     */
    public Long getCreationBy() {
        return creationBy;
    }

    /**
     * 设置创建人id 创建人id
     *
     * @param creationBy 创建人id 创建人id
     */
    public void setCreationBy(Long creationBy) {
        this.creationBy = creationBy;
    }

    /**
     * 获取创建时间 创建时间
     *
     * @return creation_date - 创建时间 创建时间
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * 设置创建时间 创建时间
     *
     * @param creationDate 创建时间 创建时间
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * 获取更新人id 更新人id
     *
     * @return update_by - 更新人id 更新人id
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人id 更新人id
     *
     * @param updateBy 更新人id 更新人id
     */
    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取更新时间 更新时间
     *
     * @return update_date - 更新时间 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间 更新时间
     *
     * @param updateDate 更新时间 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}